from django.shortcuts import render
from django.http import HttpResponse
from .models import Note

# Create your views here.
def index(request):
    args = Note.objects.all().values()
    list_result = [entry for entry in args]
    return render(request, "main/index.html", {'list_result':list_result})


def user(request, id):
    args = Note.objects.all().values()
    list_entries = [entry for entry in args]
    list_result = ""
    for user in list_entries:
        if int(id) == user.get('id'):
            list_result = [user]

    if list_result == "":
        list_result = [{'id': 'User not found'}]
        
    return render(request, "main/index.html", {'list_result':list_result})


def delete_user(request):
    try:
        args = Note.objects.all()
        args.delete()
        info = 'Users deleted successfully!'
    except:
        info = 'Something went wrong...'
    return HttpResponse(args)