# Django REST-ful API Prototype

**Este pequeño prototipo construido en Django lo creamos para una aplicación web de toma de notas de Google Keep-esque.**
**Queremos construir una API completa de REST con los puntos finales de CRUD, para crear, leer, actualizar y eliminar notas.**
**La buena noticia es que en lugar de abordar estos puntos finales individualmente, Django nos permite más o menos crearlos**
**todos de una sola vez. .**

[[_TOC_]]

> **Config: App**
- _init_.py: Inicialización de la aplicación
- settings.py: Configuración de parámetros y variables 

> **Model: Data**
- models.py: User y UserSchema
- urls.py: Routing

> **Render: Template Files (EJS)**
- index.html: Página web
- style.css: Estilos 

<img src="images/implement.png" tittle="Web Browser Inicio">
---

## 1 - Preparando nuestro Proyecto:

**De manera fácil y sencilla, sino tenemos instalado Python ya, entonces lo instalamos y creamos nuestro proyecto:**
> **Instalación de Python.**
```python
# install python 
pip install Django
django-admin startproject notable_django
cd notable_django
```

**Una vez instalado Python, el siguiente paso es instalar 'TastyPie' que es el que nos proporciona el Framework de REST.**

> **Instalación de TastyPie**

```python
pip install django-tastypie
```
**Finalmente podemos comenzar una app en nuestro proyecto:**

> **Comenzando la app**

```python
python manage.py startapp api
```

Dentro de nuestro directorio 'notable_django' deberíamos tener dos subcarpetas llamadas:
- notable_django, esta carpeta contiene la configuración del proyecto y las URLs
- api (con un documento manage.py), eta carpeta obviamente contiene toda la información que soporta la API.

**Antes que nada debemos INSTALAR nuestra app dentro de nuestro PROYECTO, dentro de 'notable_django/settings-py':**

> **Instalación de la app en nuestro proyecto**

```python
# notable_django/settings.py
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'api'
]
```
Lo importante aquí es identificar que nuestro proyecto es 'notable_django' y nuesta app que se llama 'api'.

---

## 2 - Creación del Modelo:
**Lo primero que debemos hacer es crear un modelo Note con el que interaccionar**
Antes que nada explicar lo que es un modelo: es la única y definitiva fuente de información sobre sus datos. 
Contiene los campos y comportamientos esenciales de los datos que estás almacenando. 
Generalmente, cada modelo se mapea a una sola tabla de base de datos.


> **Creación del modelo en api/models.py**

```python
# api/models.py
class Note(models.Model):
    title = models.CharField(max_length=200)
    body = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
```
Definimos correctamente los campos, teniendo en cuenta también los tipos de cada uno de ellos.

> **Lo siguiente que queremos hacer es añadir un método __str__ al modelo.**
**Este método define lo que obtenemos cuando pedimos una instancia particular de un modelo.**

```python
# api/models.py
class Note(models.Model):
    title = models.CharField(max_length=200)
    body = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
def __str__(self):
        return self.title
```

Esto significa que cuando cojamos el modelo Note, solo cogeremos el título.

**Ahora expandimos nuestro método __str__ para que también incluya el cuerpo.**

> **método str incluyendo cuerpo**
```python
# api/models.py
class Note(models.Model):
    title = models.CharField(max_length=200)
    body = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
def __str__(self):
        return '%s %s' % (self.title, self.body)
```

**Nuestro modelo está preparado, ahora solo necesitamos ejecutar las migraciones las cuales preparan la base de datos.**

> **manage.py makemigrations y migrate**

```python
python manage.py makemigrations
python manage.py migrate
```

**A continuacuón, popularemos nuestra base de datos con una sola nota, para ver que todo funciona correctamente:**

> **manage.py shell**

```python
python manage.py shell
>>> from api.models import Note
>>> note = Note(title="First Note", body="This is certainly noteworthy")
>>> note.save()
>>> Note.objects.all()
<QuerySet [<Note: First Note This is certainly noteworthy>]>
>>> exit()
```
Aquí creamos nuestra nota, la guardamos y nos devuelve todas las notas. Podemos ver también nuestro método '__str__',
el cual nos devuelve el título y el cuerpo. 

---

## 3 - Modificaciones de la API (API-ification):

**Ahora nuestro modelo y base de datos ya están listos.**
**A continuacion exponemos algunos 'endpoints' al mundo entero, para que podamos interactuar con nuestros datos. **

Un usuario hará una solicitud a un endpoint. Dependiendo del URL, 
el usuario será redirigido a un recurso en particular, que luego realizará la acción CRUD apropiada en el modelo.

**En la carpeta 'api', creamos un nuevo archivo llamado 'resources.py'**

> **api/resources.py**

```python
from tastypie.resources import ModelResource
from api.models import Note

class NoteResource(ModelResource):
    class Meta:
        queryset = Note.objects.all()
        resource_name = 'note'
```
**Importamos nuestro modelo, y creamos un recurso a partir de este mismo. El 'queryset', que es lo que al modelo**
**de recursos verdaderamente le importa, es todos los objetos nota.**

**Por lo tanto necesitamos llamar de manera apropiada a nuestro recurso: 'note'. Esto es bastante importante cuando 
**tratemos las URLs**

> **Preparamos las URLs en nuestro archivo notable_django/urls.py:** 


```python
from django.conf.urls import url, include
from django.contrib import admin
from api.resources import NoteResource
note_resource = NoteResource()
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(note_resource.urls)),
]
```
Importamos nuestro NoteResource, lo instanciamos, y después le decimos que queremos todas las URLs que 
comiencen por 'api/' para que nos redirija al recurso. Importante importar 'inlcude from django.conf.urls'.

---

## 4 - Testeando nuestra API:

**Finalmente ya podemos comenzar a ejecutarlo. Para esto usaremos 'Postman' (recomendado en el Node tutorial, link en la bibliografía) 
** para hacer requests a nuestra API. Después de descargar Postman, ejecutamos el siguiente comando**

**Para ejecutar correctamente esto, hay que meterse en la carpeta 'src' y lanzar este comando**
> ** runserver**

```python
python manage.py runserver
```
> **Ejecución correcta**
<img src="images/runserver.png" tittle="RUNSERVER">

>**Ahora en Postman, le hacemos un GET request a esta URL: 'http://localhost:8000/api/note/1':**
<img src="images/get.png" tittle="GET REQUEST">

Ahora nuestro 'endpoint' funciona perfectamente.
A continuación intentaremos crear una nueva nota desde Postman.

---

## 5 - POST, PUT, DELETE:

**Aquí enseñamos un ejemplo de un post, donde se ha escrito una nota y se ha añadido**
Enviamos un JSON con la opción RAW como nuestra nueva nota:

<img src="images/post.png" tittle="POST">

Hay que asegurarse de seleccionar la opción JSON en el menú deplegable, además de enviar la
solicitud a 'http://localhost:8000/api/note/'

Si el envío de la petición falla y nos devuelve un error 401 (No Autorizado). TastyPie protege sus modelos
fuera, y solo permite LEER, no modificar los datos.

**Esto se puede arreglar de manera fácil, importamos la clase de Autorización Base, y la añadimos a nuestros recursos**

---

## 6 - Todos los ENDPOINTS:
**Bien, entonces terminamos nuestros endpoints GET y POST. ¿Qué ha pasado con PUT y BORRAR?**
Bueno, la magia de TastyPie es que ya están hechos. Intenta actualizar o borrar tu primera nota terminando PUT o DELETE
 en http://localhost:8000/api/note/1/. ¡Simplemente funciona!
Así de simple, creamos una API REST que funciona.

**Después de haber completado todo, el orden de los datos de las carpetas debe quedar de esta forma:**

> **CARPETA PROYECTO**
<img src="images/project_dir.png" tittle="Carpeta Proyecto">

> **CARPETA API**
<img src="images/api_dir.png" tittle="DIRECTORIO API">

> **Mostrar Usuarios: WebBrowser [GET]**
<img src="images/notable_django_dir.png" tittle="Directorio Notable Django ">
<img src="images/content_front.png" tittle="Content Front">
---

## 7 - Bibliografía:
**- Django**: https://codeburst.io/create-a-django-api-in-under-20-minutes-2a082a60f6f3  
**- Postman**: https://www.postman.com/  
**- TastyPie**: https://github.com/django-tastypie/django-tastypie  
**- Django y los Modelos**: https://docs.djangoproject.com/en/1.10/topics/db/models/  