# Flask API Prototype

**Este pequeño prototipo construido en Flask se encarga de almacenar en una pequeña base de datos un conjunto de usuarios, los cuales introducen sus datos personales mediante una aplicación externa con el fin de ser gestionados por un administrador para publicarlos en una página web.**

[[_TOC_]]

> **Config: App**
- my_app.py: Inicialización de la aplicación
- config.py: Configuración de parámetros y variables 

> **Model: Data**
- models.py: User y UserSchema
- views.py: Routing

> **Render: Template Files (EJS)**
- index.html: Página web
- style.css: Estilos

---

<img src="images/content_front.PNG" tittle="Cards">

---

## 1 - Instalar dependencias y entornos:
```python
# install flask 
pip install flask
# install Flask-SQLAlchemy
pip install flask_sqlalchemy
# install Flask-Marshmallow
pip install flask_marshmallow
# install marshmallow-sqlalchemy
pip install marshmallow-sqlalchemy
# install flask_paginate
pip install flask_paginate
```
Una vez instaladas todas las librerias y herramientas necesarias, vamos a describir el proceso de creación y ejecución paso a paso.

---

## 2 - Configuración de la API (Flask):
**Primero, tenemos que declarar las importaciones de nuestras dependencias y entornos descargados previamente para poder trabajar con ellos:**

> **my_app.py**

```python
from flask import Flask,request,jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import os
```

**Segundo, tenemos que inicializar nuestra aplicación flask y configurar la localización de nuestro SQLAlchemy para poder generar el objeto SQLAlchemy junto a Marshmallow:**

> **my_app.py**

```python
# Set up app configuration and declare serialization with SQL
app = Flask(__name__,  template_folder= "app/templates", static_folder="app/static")
app.config.from_pyfile('config.py')
db = SQLAlchemy(app)
ma = Marshmallow(app)
meta = MetaData()
```

**Tercero, establecemos los parámetros de configuración y las variables específicas de nuestra aplicación:**

> **config.py**

```python
import os

basedir = os.path.abspath(os.path.dirname(__file__))
DEBUG = True
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir,'curd.sqlite')
SECRET_KEY = 'My_secret_key'
```

---

## 3 - Creación de los modelos (Flask):

**Una vez que tenemos inicializados nuestros objetos, es necesario construir una clase Modelo para Usuarios los cuales van a estar compuestos por (group, url, username, email), para ello vamos a crear una nueva carpeta llamada 'app' la cual va a contener nuestros modelos de datos y nuestras vistas para poder renderizar nuestro contenido como veremos posteriormente:**

> **Estructura de archivos**

```python
config.py
my_app.py
/app
  |
  |--- /static
          |--- /css
                 |--- style.css
  |             
  |--- /templates
            |--- index.html
  |
  |--- __init__.py
  |--- models.py
  |--- views.py
```

> **models.py**

```python
from my_app import db, ma

class User(db.Model):
    username = db.Column(db.String(120), primary_key=True)
    url = db.Column(db.String(120), unique=True)
    email =  db.Column(db.String(120), unique=True)
    group = db.Column(db.String(120))

    def __init__(self, username, url, email, group):
        self.username = username
        self.url = url
        self.email = email
        self.group = group

class UserSchema(ma.Schema):
    class Meta:
        fields = ('username', 'url', 'email', 'group')
```

---

## 4 - Creación de rutas (Flask):

**Una vez construido nuestro modelo de datos podemos pasar a construir y declarar los 'endpoints' de la aplicación, para ello vamos a declarar tanto las rutas como el método utilizado para poder realizar diferentes funciones:**

> **views.py**
```python
from my_app import app
from app.models import User, UserSchema
from flask import Flask, request, jsonify, render_template
from flask_paginate import Pagination, get_page_args
from my_app import app, db

db.create_all()
user_schema = UserSchema()
group_schema = UserSchema(many=True)

...

```

> **Ruta y función para añadir un usuario en nuestra base de datos utilizando un mapeo JSON y devolverlo si se ha incorporado correctamente. [POST]**

```python
...

@app.route("/adduser", methods=["POST"])
def add_user():
    username = request.json['username']
    url = request.json['url']
    email = request.json['email']
    group = request.json['group']

    new_user = User(username, url, email, group)

    db.session.add(new_user)
    db.session.commit()
    new_user_data = {"username":new_user.username, "url":new_user.url, "email":new_user.email, "group":new_user.group}

    return jsonify(new_user_data)

...
```

> **Ruta y función para mostrar el contenido de todos los usuarios que se han registrado, incorporando Paginación y Renderizado de Templates. [GET]**

```python
...

@app.route("/groups", methods=['GET'])
def get_group():
    all_users = User.query.all()
    result = group_schema.dump(all_users)
    pagination = Pagination(page=1, per_page=1, total=1,
                        css_framework='bootstrap4')
    return render_template('index.html', result = result, pagination=pagination)

...
```

> **Ruta y función para mostrar el contenido del usuario cuyo "Username" ha sido especificado. [GET]**

```python
...

@app.route("/user/<username>", methods=["GET"])
def user_details(username):
    user = User.query.get(username)
    return user_schema.jsonify(user)

...
```

> **Ruta y función para borra todo el contenido de nuestra base de datos y generar un nuevo esquema a partir del declarado. [POST]** 

```python
...

@app.route("/delete", methods=["POST"])
def delete_users():
    try:
        db.drop_all()
        db.create_all()
    except:
        db.session.rollback()
    return "Table {} successfully deleted.".format(User)
```

---

## 5 - Construcción de la API (Templates)
**Declarado todo el código de nuestra aplicación el cual va a manejar todos los datos desde el back-end, podemos implementar un pequeño front-end para mostrar el contenido de nuestros usuarios:**
- Crear una nueva carpeta llamada templates la cual va a contener el código html
- Crear una nueva carpeta static/css la cual va a contener el código css

> **Templates: index.html**
El codigo declarado tanto al principio como al final de index.html parte de la base de cualquier archivo html, por lo tanto vamos a pasar a explicar el contenido relacionado con la gestión de los datos con Flask:
```html
...

{% for item in result %}
    <section class="seccion" id="1">
        <p><b>GROUP</b>: {{ item["group"]}}</p> 
        <p><b>URL</b>: {{ item["url"]}}</p> 
        <p><b>USERNAME</b>: {{ item["username"]}}</p> 
        <p><b>EMAIL</b>: {{ item["email"]}}</p> 
        </br>
    </section>
{% endfor %}

...
```
**Para poder mostrar el contenido de nuestros datos es necesario gestionar los datos que nos llegan con el fin de mostrar los datos que queramos, para ello utilizamos la sintaxis de Flask {%} para añadir funciones y código Python.**

> **Static: style.css**
El codigo declarado en el archivo se puede implementar e incorporar en cualquier documento, se puede tomar el incorporado en el proyecto.


---

## 6 - Inicialización de la API:
**Eso es todo, una vez que hemos añadido las diferentes funciones con las que poder interactuar con nuestra API, es necesario declarar el "Entry Point" e inicializar la base de datos:**

```python
...

from app.views import * 

def main():
    app.run(host='0.0.0.0', port=8080, debug=True)

if __name__ == '__main__':
    main()
```

> **shell**
```shell
python my_app.py
```

**Para iniciar nuestro servidor Flask con el puerto 8080 y que sea accesible con localhost:8080 podemos utilizar nuestro propio navegador WEB o utilizar la herramienta POSTMAN para realizar llamadas mediante GET, POST, PUT, DELETE, según necesitemos.**

> **Añadir Usuario: POSTMAN [POST]**
<img src="images/postman_adduser.PNG" tittle="User">

> **Reiniciar Base de Datos: POSTMAN [POST]**
<img src="images/postman_deletedb.PNG" tittle="DB">

> **Mostrar Usuarios: WebBrowser [GET]**
<img src="images/content_front.PNG" tittle="Cards">

## 7 - Bibliografía:
**- Flask**: https://flask.palletsprojects.com/en/1.1.x  
**- Postman**: https://www.postman.com/  
**- Marshmallow**: https://flask-marshmallow.readthedocs.io/en/latest/  
**- SQLAlchemy**: https://flask-sqlalchemy.palletsprojects.com/en/2.x/  
