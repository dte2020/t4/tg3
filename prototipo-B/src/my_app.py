from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from sqlalchemy import MetaData

# Set up app configuration and declare serialization with SQL
app = Flask(__name__,  template_folder= "app/templates", static_folder="app/static")
app.config.from_pyfile('config.py')
db = SQLAlchemy(app)
ma = Marshmallow(app)
meta = MetaData()


from app.views import * 

def main():
    app.run(host='0.0.0.0', port=8080, debug=True)

if __name__ == '__main__':
    main()