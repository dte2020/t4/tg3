from my_app import app
from app.models import User, UserSchema
from flask import Flask, request, jsonify, render_template
from flask_paginate import Pagination, get_page_args
from my_app import app, db

db.create_all()
user_schema = UserSchema()
group_schema = UserSchema(many=True)

@app.route("/adduser", methods=["POST"])
def add_user():
    username = request.json['username']
    url = request.json['url']
    email = request.json['email']
    group = request.json['group']

    new_user = User(username, url, email, group)

    db.session.add(new_user)
    db.session.commit()
    new_user_data = {"username":new_user.username, "url":new_user.url, "email":new_user.email, "group":new_user.group}

    return jsonify(new_user_data)


@app.route("/groups", methods=["GET"])
def get_group():
    all_users = User.query.all()
    result = group_schema.dump(all_users)
    pagination = Pagination(page=1, per_page=1, total=1,
                        css_framework='bootstrap4')
    return render_template('index.html', result = result, pagination=pagination)


@app.route("/user/<username>", methods=["GET"])
def user_details(username):
    user = User.query.get(username)
    return user_schema.jsonify(user)


@app.route("/delete", methods=["POST"])
def delete_users():
    try:
        db.drop_all()
        db.create_all()
    except:
        db.session.rollback()
    return "Table {} successfully deleted.".format(User)

