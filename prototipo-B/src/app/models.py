from my_app import db, ma

class User(db.Model):
    username = db.Column(db.String(120), primary_key=True)
    url = db.Column(db.String(120), unique=True)
    email =  db.Column(db.String(120), unique=True)
    group = db.Column(db.String(120))

    def __init__(self, username, url, email, group):
        self.username = username
        self.url = url
        self.email = email
        self.group = group

class UserSchema(ma.Schema):
    class Meta:
        fields = ('username', 'url', 'email', 'group')



